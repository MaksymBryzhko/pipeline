package org.pipeline;

import org.pipeline.proc.Processor;
import org.pipeline.proc.ProcessorFactory;

import java.util.Map;

/**
 * Executes pipeline by given {@link PipelineDescriptor}.
 */
public class PipelineExecutor {

    private final ProcessorFactory processorFactory;

    /*
    Collection<ExecutionInstance> executors;

    ExecutionInstance {
        Thread executorThread;
        long startingTimeMs;
    }

    class MonitorThread implements Runnable {

        public void run() {
            while (true) {
                for (ExecutionInstance i : executors) {
                    if (System.currentMs - i.startingTimeMs > maxTransformTimeMs) {
                        i.executorThread.interrupt();
                        remove
                    }

                }
                Thread.sleep(100);
            }
        }

    }
     */



    public PipelineExecutor(ProcessorFactory processorFactory, long maxTransformTimeMs) {
        this.processorFactory = processorFactory;

        this.executors = new ArralList();
        new Thread(new MonitorThread()).start();
    }

    public void transform(PipelineDescriptor pipelineDescriptor, Map<String, Object> jsonDocument) {
        /*
         executors.add(new ExecutionInstance(Thread.currentThread(), System.currentMs()))
         */
        pipelineDescriptor.getSteps().stream()
                .map(this::createAndInitProcessor)
                .forEach(processor -> transformJson(processor, jsonDocument));

    }

    private Processor createAndInitProcessor(PipelineDescriptor.Step step) {
        Processor processor = processorFactory.create(step.getProcessor());
        processor.initialize(step.getConfiguration());
        return processor;
    }

    private void transformJson(Processor processor, Map<String, Object> jsonDocument) {
        processor.process(jsonDocument);
    }
}
