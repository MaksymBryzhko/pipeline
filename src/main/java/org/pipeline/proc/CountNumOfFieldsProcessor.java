package org.pipeline.proc;

import java.util.Map;

/**
 * Implements processor that adds number of fields as a separate field.
 */
public class CountNumOfFieldsProcessor implements Processor {
    public static final String TARGET_FIELD_NAME = "targetFieldName";

    private String fieldName;

    @Override
    public void initialize(Map<String, String> configuration) {
        this.fieldName = configuration.get(TARGET_FIELD_NAME);

        if (fieldName == null) {
            throw new IllegalArgumentException("'targetFieldName' should be specified");
        }
    }

    @Override
    public void process(Map<String, Object> jsonDocument) {
        int count = jsonDocument.size();
        jsonDocument.put(fieldName, count);
    }
}
