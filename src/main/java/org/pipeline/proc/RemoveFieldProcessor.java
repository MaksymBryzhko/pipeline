package org.pipeline.proc;

import java.util.Map;

/**
 * Implements processors that removes field.
 */
public class RemoveFieldProcessor implements Processor {
    public static final String FIELD_NAME = "fieldName";

    private String fieldName;

    @Override
    public void initialize(Map<String, String> configuration) {
        this.fieldName = configuration.get(FIELD_NAME);

        if (fieldName == null) {
            throw new IllegalArgumentException("'fieldName' should be specified");
        }
    }

    @Override
    public void process(Map<String, Object> jsonDocument) {
        jsonDocument.remove(fieldName);
    }
}
