package org.pipeline.proc;

import java.util.function.Supplier;

public class DefaultProcessorFactory implements ProcessorFactory {



    @Override
    public Processor create(String processorName) {
        Processors processors = Processors.valueOf(processorName);

        return new CachingProcessor(processors.creator, cache);

//        return Processors.valueOf(processorName).creator.get();
    }

    /**
     * Enumerates all processors and their creators;
     */
    private enum Processors {
        AddField(AddFieldProcessor::new),
        RemoveField(RemoveFieldProcessor::new),
        CountNumOfFields(CountNumOfFieldsProcessor::new);

        Processors(Supplier<Processor> creator) {
            this.creator = creator;
        }

        Supplier<Processor> creator;
    }

    @interface PipelineProcessor {
        String value = null;
    }




}
