package org.pipeline.proc;

import java.util.Map;

/**
 * Implements processor that adds field.
 */
@DefaultProcessorFactory.PipelineProcessor("name")
public class AddFieldProcessor implements Processor {
    public static final String FIELD_NAME = "fieldName";
    public static final String FIELD_VALUE = "fieldValue";

    private String fieldName;
    private Object fieldValue;

    @Override
    public void initialize(Map<String, String> configuration) {
        fieldName = configuration.get(FIELD_NAME);
        fieldValue = configuration.get(FIELD_VALUE);

        if (fieldName == null || fieldValue == null) {
            throw new IllegalArgumentException("'fieldName' and 'fieldValue' should be specified");
        }
    }

    @Override
    public void process(Map<String, Object> jsonDocument) {
        jsonDocument.put(fieldName, fieldValue);
    }
}
