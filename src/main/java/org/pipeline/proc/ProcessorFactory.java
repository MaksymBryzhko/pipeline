package org.pipeline.proc;

public interface ProcessorFactory {
    Processor create(String processorName);
}
