package org.pipeline.proc;

import java.util.Map;
import java.util.function.Supplier;

public class CachingProcessor implements Processor {

    private Supplier<Processor> newTargetProcessor;
    private Cache<String, Processor> cache;

    private Processor targetProcessor;


    @Override
    public void initialize(Map<String, String> configuration) {

        String cachingKey = generateKey(configuration);

        if (/* cachingKey in not cache*/) {
            Processor newP = newTargetProcessor.get();
            // put newP into a cache
            newP.initialize(configuration);
            targetProcessor = newP;
        }
    }

    @Override
    public void process(Map<String, Object> jsonDocument) {
        targetProcessor.process(jsonDocument);
    }
}
