package org.pipeline;

import lombok.Value;

import java.util.List;
import java.util.Map;

/**
 * Pipeline descriptor that represents configuration of a pipeline.
 */
@Value
public class PipelineDescriptor {
    List<Step> steps;

    @Value
    static class Step {
        String processor;
        Map<String, String> configuration;
    }
}

