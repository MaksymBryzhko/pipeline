package org.pipeline;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.pipeline.proc.DefaultProcessorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PipelineExecutorTest {

    private PipelineExecutor executor;
    private PipelineDescriptor descriptor;
    private DefaultProcessorFactory processorFactory;

    @BeforeEach
    void setup() throws IOException {
        processorFactory = new DefaultProcessorFactory();
        executor = new PipelineExecutor(processorFactory);
        descriptor = new ObjectMapper().readValue(
                PipelineDescriptorTest.class.getResourceAsStream("/sanityTestDescriptor.json"),
                PipelineDescriptor.class);
    }

    @Test
    void doSanityTest() {
        // given
        Map<String, Object> json = new HashMap<>();
        json.put("Hello", "World");
        json.put("Good", "Morning");
        json.put("Company", "Logz.io");

        // when
        executor.transform(descriptor, json);

        // Then
        assertEquals(5, json.size());
        assertEquals("George", json.get("firstName"));
        assertEquals(4, json.get("numOfFields"));
    }
}