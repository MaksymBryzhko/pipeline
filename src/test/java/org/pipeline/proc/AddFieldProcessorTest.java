package org.pipeline.proc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class AddFieldProcessorTest {

    private AddFieldProcessor processor;

    @BeforeEach
    void setup() {
        processor = new AddFieldProcessor();
    }

    @Test
    public void processorCanAddField() {
        // Given
        var config = Map.of("fieldName", "accountName",
                "fieldValue", "Facebook");

        // When
        var json = new HashMap<String, Object>();
        processor.initialize(config);
        processor.process(json);

        // Then
        assertEquals("Facebook", json.get("accountName"));
    }

    @Test
    public void errorWhenConfigurationIfWrong() {
        // Given
        var config = Map.of("fieldName", "accountName",
                "wrongField", "Facebook");

        // When-Then
        var json = new HashMap<String, Object>();
        Assertions.assertThrows(IllegalArgumentException.class, () -> processor.initialize(config));
    }
}