package org.pipeline;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class PipelineDescriptorTest {

    private ObjectMapper mapper;

    @BeforeEach
    void setup() {
        mapper = new ObjectMapper();
    }

    @Test
    void pipelineDescriptorCanBeParsed() throws IOException {
        // Given
        InputStream testDescStream = PipelineDescriptorTest.class.getResourceAsStream("/testDescriptor.json");

        // When
        PipelineDescriptor desc = mapper.readValue(testDescStream, PipelineDescriptor.class);

        // Then
        assertEquals(4, desc.getSteps().size());
        assertEquals("AddField", desc.getSteps().get(0).getProcessor());
        assertEquals("accountName", desc.getSteps().get(0).getConfiguration().get("fieldName"));
        assertEquals("Facebook", desc.getSteps().get(0).getConfiguration().get("fieldValue"));
    }
}